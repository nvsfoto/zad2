# Wstęp

Obecnie powszechnie wykorzystuje się języki ze znacznikami do opisania dodatkowych informacji umieszczanych w plikach tekstowych. Z pośród najbardziej popularnych można wspomnieć o:

1. **html** – służącym do opisu struktury informacji zawartych na stronach internetowych,
2. **Tex** (Latex) – poznany na zajęciach język do „profesjonalnego” składania tekstów,
3. **XML** (*Extensible Markup Language*) - uniwersalnym języku znaczników przeznaczonym do reprezentowania różnych danych w ustrukturalizowany sposób.

Przykład kodu html i jego interpretacja w przeglądarce:

<table>
  <tbody>
    <tr>
    	<th>HTML</th>
        <th>Podgląd</th>
    </tr>
    <tr>
      <td align="left">
         <pre>   
<code>&lt;!DOCTYPE <b>html</b>>
&lt;<b>html</b>>
    &lt;<b>head</b>>
        &lt;<b>meta</b> charset=<span style="color:red">"utf-8"</span> />
        &lt;title>Przykład&lt;/title>
    &lt;/<b>head</b>>
    &lt;<b>body</b>>
        &lt;p> Jakiś paragraf tekstu&lt;/<b>p</b>>
    &lt;/<b>body</b>>
&lt;/<b>html</b>>
</code>
         </pre>
      </td>
      <td align="center"><img src="html_preview.png" alt="HTML Preview"/></td>
    </tr>
  </tbody>
</table>


Przykład kodu Latex i wygenerowanego pliku w formacie pdf:

<table>
  <tbody>
	<tr>
    	<th>HTML</th>
        <th>Podgląd</th>
    </tr>
    <tr>
      <td align="left">
         <pre>   
<code>\<span style="color:#800020">documentclass</span>[]{<span style="color:blue;">letter</span>}
\<span style="color:#800020">usepackage</span>{<span style="color:blue;">lipsum</span>}
\<span style="color:#800020">usepackage</span>{<span style="color:blue;">polyglossia</span>}
\<span style="color:#800020">setmainlanguage</span>{<span style="color:blue;">polish</span>}
\<span style="color:blue;font-weight: 900;">begin</span>{<span style="color:blue;font-weight: 900;">document</span>}
\<span style="color:blue;font-weight: 900;">begin</span>{<span style="color:blue;font-weight: 900;">letter</span>}{<span style="color:blue;font-weight: 900;">Szanowny Panie XY</span>}
\<span style="color:#800020">address</span>{<span style="color:blue;">Adres do korespondencji</span>}
\<span style="color:#800020">opening</span>{}
\<span style="color:#800020">lipsum</span>[2]
\<span style="color:#800020">signature</span>{<span style="color:blue;">Nadawca</span>}
\<span style="color:#800020">closing</span>{<span style="color:blue;">Pozdrawiam</span>}
\<span style="color:blue;font-weight: 900;">end</span>{<span style="color:blue;font-weight: 900;">letter</span>}
\<span style="color:blue;font-weight: 900;">end</span>{<span style="color:blue;font-weight: 900;">document</span>}
</code>
         </pre>
      </td>
      <td align="center"><img src="latex_preview.png" alt="Latex Preview"/></td>
    </tr>
  </tbody>
</table>


Przykład kodu XML – fragment dokumentu SVG (Scalar Vector Graphics):

<table>
  <tbody>
    <tr>
    	<th>HTML</th>
        <th>Podgląd</th>
    </tr>
    <tr>
      <td align="left">
         <pre>   
<code>&lt;!DOCTYPE <b>html</b>>
&lt;<b>html</b>>
   &lt;<b>body</b>>
   		&lt;svg height=<span style="color:red">"100"</span> width=<span style="color:red">"100"</span>
			&lt;circle cx=<span style="color:red">"50"</span> cy=<span style="color:red">"50"</span> r=<span style="color:red">"40"</span> stroke=<span style="color:red">"black"</span> stroke-width=<span style="color:red">"3"</span> fill=<span style="color:red">"red"</span> />
		&lt;/svg>
	&lt;/<b>body</b>>
&lt;/<b>html</b>>
</code>
         </pre>
      </td>
      <td align="center">
          <svg height="100" width="100">
			<circle cx="50" cy="50" r="40" stroke="black" stroke-width="3" fill="red" />
		</svg></td>
    </tr>
  </tbody>
</table>

W tym przypadku mamy np. znacznik np. \<circle> opisujący parametry koła i który może być
właściwie zinterpretowany przez dedykowaną aplikację (np. przeglądarki www).

Jako ciekawostkę można podać fakt, że również pakiet MS Office wykorzystuje format XML do
przechowywania informacji o dodatkowych parametrach formatowania danych. Na przykład pliki z
rozszerzeniem docx, to nic innego jak spakowane algorytmem zip katalogi z plikami xml.

```shell
$unzip -l test.docx
Archive: test.docx
Length Date Time Name
--------- ---------- ----- ----
573 2020-10-11 18:20 _rels/.rels
731 2020-10-11 18:20 docProps/core.xml
508 2020-10-11 18:20 docProps/app.xml
531 2020-10-11 18:20 word/_rels/document.xml.rels
1421 2020-10-11 18:20 word/document.xml
2429 2020-10-11 18:20 word/styles.xml
853 2020-10-11 18:20 word/fontTable.xml
241 2020-10-11 18:20 word/settings.xml
1374 2020-10-11 18:20 [Content_Types].xml
```

Wszystkie te języki znaczników cechują się rozbudowaną i złożoną składnią i dlatego do ich edycji
wymagają najczęściej dedykowanych narzędzi w postaci specjalizowanych edytorów. By
wyeliminować powyższą niedogodność powstał Markdown - uproszczony język znaczników
służący do formatowania dokumentów tekstowych (bez konieczności używania specjalizowanych
narzędzi). Dokumenty w tym formacie można bardzo łatwo konwertować do wielu innych
formatów: np. html, pdf, ps (postscript), epub, xml i wiele innych. Format ten jest powszechnie
używany do tworzenia plików README.md (w projektach open source) i powszechnie
obsługiwany przez serwery git’a. Język ten został stworzony w 2004 r. a jego twórcami byli John
Gruber i Aaron Swartz. W kolejnych latach podjęto prace w celu stworzenia standardu rozwiązania
i tak w 2016 r. opublikowano dokument <u>RFC 7764</u> który zawiera opis kilku odmian tegoż języka:

- CommonMark,
- GitHub Flavored Markdown (GFM),
- Markdown Extra.

# Podstawy składni

Podany link: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet zawiera opis
podstawowych elementów składni w języku angielskim. Poniżej zostanie przedstawiony ich krótki
opis w języku polskim.



## Definiowanie nagłówków

W tym celu używamy znaku kratki

Lewe okno zawiera kod źródłowy – prawe -podgląd przetworzonego tekstu

<img align="left" width="100%" height="auto" src="headers_preview.png">

## Definiowanie list



<img align="left" width="100%" height="auto" src="list_preview.png">

Listy numerowane definiujemy wstawiając numery kolejnych pozycji zakończone kropką.

Listy nienumerowane definiujemy znakami: *,+,-

## Wyróznianie tekstu

<img align="left" width="100%" height="auto" src="formatting_preview.png">

## Tabele

<img align="left" width="100%" height="auto" src="table_preview.png">

Centrowanie zawartości kolumn realizowane jest poprzez odpowiednie użycie znaku dwukropka:

## Odnośniki do zasobów

\[odnośnik do zasobów](www.gazeta.pl)
\[odnośnik do pliku](LICENSE.md)
\[odnośnik do kolejnego zasobu][1]

\[1]: http://google.com

## Obrazki

\![alt text](https://server.com/images/icon48.png "Logo 1") – obrazek z zasobów
internetowych
\![](logo.png) – obraz z lokalnych zasobów

## Kod źródłowy dla różnych języków programowania



<img align="left" width="100%" height="auto" src="code_preview.png">

## Tworzenie spisu treści na podstawie nagłówków



<img align="left" width="100%" height="auto" src="table_of_content_preview.png">



# Edytory dedykowane

Pracę nad dokumentami w formacie Markdown( rozszerzenie md) można wykonywać w
dowolnym edytorze tekstowym. Aczkolwiek istnieje wiele dedykowanych narzędzi

1. Edytor Typora - https://typora.io/
2. Visual Studio Code z wtyczką „markdown preview”

<img align="left" width="100%" height="auto" src="typora.png">



# Pandoc – system do konwersji dokumentów Markdown do innych formatów

Jest oprogramowanie typu open source służące do konwertowania dokumentów
pomiędzy różnymi formatami.

Pod poniższym linkiem można obejrzeć przykłady użycia:

https://pandoc.org/demos.html

Oprogramowanie to można pobrać z spod adresu: https://pandoc.org/installing.html

Jeżeli chcemy konwertować do formatu latex i pdf trzeba doinstalować oprogramowanie
składu Latex (np. Na windows najlepiej sprawdzi się Miktex: https://miktex.org/)

Gdyby podczas konwersji do formatu pdf pojawił się komunikat o niemożliwości
znalezienia programu pdflatex rozwiązaniem jest wskazanie w zmiennej środowiskowej
PATH miejsca jego położenia

<img align="center"  src="settings.png">

<img align="center"  src="envs.png">

<img align="center"  src="edit_envs.png">



Pod adresem (https://gitlab.com/mniewins66/templatemn.git) znajduje się przykładowy plik
Markdown z którego można wygenerować prezentację w formacie pdf wykorzystując
klasę latexa beamer.

W tym celu należy wydać polecenie z poziomu terminala:

$pandoc templateMN.md -t beamer -o prezentacja.pdf